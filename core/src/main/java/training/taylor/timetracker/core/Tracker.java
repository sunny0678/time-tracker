package training.taylor.timetracker.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import training.taylor.timetracker.core.dao.TimeEntry;

import java.util.List;

@Component
public class Tracker {
	@Autowired
	private List<TimeEntry> entries;

	/**
	 * add
	 * 
	 * @param entry
	 */
	public final void add(final TimeEntry entry) {
		entries.add(entry);
	}

	/**
	 * remove
	 * 
	 * @param entry
	 */
	public final void remove(final TimeEntry entry) {
		if (entry != null) {
			entries.remove(entry);
		}
	}

	/**
	 * size
	 * 
	 * @return Tracker size
	 */
	public final int size() {
		if (entries != null) {
			return entries.size();
		}
		return 0;
	}

	/**
	 * TimeEntry get
	 * 
	 * @param index
	 * @return TimeEntry
	 */
	public final TimeEntry get(final int index) {
		return entries.get(index);
	}
}
